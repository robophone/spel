package setting;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class GameSettings {
	
	public Keybinds keybinds = new Keybinds();
	
	public class Keybinds {
		
		public ArrayList<Keybind> keyBinds = new ArrayList<Keybind>();
		
		public Keybind keyBindUp = new Keybind(KeyEvent.VK_W);
		public Keybind keyBindDown = new Keybind(KeyEvent.VK_S);
		public Keybind keyBindRight = new Keybind(KeyEvent.VK_D);
		public Keybind keyBindLeft = new Keybind(KeyEvent.VK_A);
		public Keybind keyBindSpace = new Keybind(KeyEvent.VK_SPACE);
		
		public Keybinds() {
			keyBinds.add(keyBindUp);
			keyBinds.add(keyBindDown);
			keyBinds.add(keyBindRight);
			keyBinds.add(keyBindLeft);
			keyBinds.add(keyBindSpace);
		}
		
		public void onKeyPress(KeyEvent e) {
			for(Keybind bind : keyBinds) {
				if(bind.keyCode == e.getKeyCode()) {
					bind.pressed = true;
				}
			}
		}
		
		public void onKeyRelease(KeyEvent e) {
			for(Keybind bind : keyBinds) {
				if(bind.keyCode == e.getKeyCode()) {
					bind.pressed = false;
				}
			}
		}
		
	}
	
}
