package physics;

public class Physics {

	public static double getAcceleration(double force, double mass, ForceType forceType) {
		if(forceType == ForceType.IGNORE_MASS) {
			mass = 1;
		}
		return force / mass;
	}
	
}
