package physics.collider;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

public class BoxCollider extends Collider {
	
	public double x, y, z, width, height, rotation;
	
	public BoxCollider(double x, double y, double z, double width, double height) {
		setPosition(x, y, z);
		setSize(width, height);
	}
	
	public void setPosition(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void setRotation(double rotation) {
		this.rotation = rotation;
	}
	
	public void setSize(double width, double height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Funkar inte som det ska men vafan spelar ingen roll det �r n�ra
	 */
	public static boolean intersects(BoxCollider box1, BoxCollider box2) {
		
		double reduction = 4;
		Shape a1 = new Rectangle2D.Double(box1.x + reduction, box1.y + reduction, box1.width - reduction, box1.height - reduction);
		Shape a2 = new Rectangle2D.Double(box2.x + reduction, box2.y + reduction, box2.width - reduction, box2.height - reduction);
		
		AffineTransform box1trans = new AffineTransform();
		AffineTransform box2trans = new AffineTransform();
		
		box1trans.rotate(Math.toRadians(box1.rotation), box1.width / 2d, box1.height / 2d);
		box2trans.rotate(Math.toRadians(box2.rotation), box2.width / 2d, box2.height / 2d);
		
		a1 = box1trans.createTransformedShape(a1);
		a2 = box2trans.createTransformedShape(a2);
		
		return a1.intersects(a2.getBounds()) && a2.intersects(a1.getBounds());
	}
	
}
