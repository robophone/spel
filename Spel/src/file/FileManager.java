package file;

public class FileManager {
	
	private FileSystemEntry[] files;
	
	public FileEntry fontFile;
	
	public FileManager() {
		files = new FileSystemEntry[] {
			new DirectoryEntry(".", "gameFiles"),
			fontFile = new FileEntry("gameFiles", "font.ttf"),
		};
	}
	
}
