package file;

import java.io.File;

public class DirectoryEntry extends FileSystemEntry {

	public DirectoryEntry(String parent, String child) {
		file = new File(parent, child);
		if(!file.exists()) {
			file.mkdirs();
		}
	}
	
}
