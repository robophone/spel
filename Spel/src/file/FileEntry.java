package file;

import java.io.File;
import java.io.IOException;

public class FileEntry extends FileSystemEntry {
	
	public FileEntry(String parent, String child) {
		file = new File(parent, child);
		if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
