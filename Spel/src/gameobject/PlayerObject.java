package gameobject;

import renderer.Sprites;

public class PlayerObject extends LivingObject {
	
	public int direction = 0;

	public PlayerObject(double x, double y, double z, double maxHealth) {
		super(x, y, z, maxHealth);
	}
	
	@Override
	public void init() {
		super.init();
		setSprite(Sprites.SHIP_SPRITE);
	}

}
