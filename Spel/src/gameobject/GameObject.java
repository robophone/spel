package gameobject;

import main.Game;
import physics.ForceType;
import physics.Physics;
import physics.collider.BoxCollider;
import renderer.Sprite;

public class GameObject {
	
	protected Game game = Game.getInstance();
	
	public GameObject(double x, double y) {
		init();
		this.x = x;
		this.y = y;
		this.preX = x;
		this.preY = y;
	}
	
	public GameObject(double x, double y, double z) {
		this(x, y);
		this.z = z;
		this.preZ = z;
	}
	
	public void init() {
		
	}
	
	private int lifetime;
	
	public double x, y, z, preX, preY, preZ, preRotation, rotation, motionX, motionY, motionZ, motionRotation;
	public double mass = 1;
	
	private BoxCollider collider;
	
	private boolean collided, isStatic = false;
	private boolean collideCheck = true;
	
	private Sprite sprite;
	
	public void preUpdate() {
		lifetime++;
		
		if(!isStatic || (isStatic && isFirstUpdate())) {
			preRotation = rotation;
			preX = x;
			preY = y;
			preZ = z;
		}
	}
	
	public void update() {
		
	}

	public void collisionUpdate() {
		if(!canCollideCheck()) {
			return;
		}
		for(GameObject obj : game.world.gameObjects) {
			if(!obj.canCollideCheck() || obj.equals(this)) {
				continue;
			}
			collided = obj.collided = (z == obj.z && BoxCollider.intersects(getCollider(), obj.getCollider()));
			if(collided) {
				onCollision(obj);
				obj.onCollision(this);
			}
		}
	}
	
	public void physicsUpdate() {
		if(isStatic) {
			return;
		}
		motionRotation *= 0.8;
		
		x += motionX;
		y += motionY;
		z += motionZ;
		
		rotation += motionRotation;
		
		if(canCollideCheck()) {
			getCollider().setPosition(x, y, z);
			getCollider().setRotation(rotation);
		}
	}
	
	public void onCollision(GameObject obj) {
		motionX = 0;
		motionY = 0;
		motionZ = 0;
		motionRotation = 0;
	}
	
	public void addForceAngle(double forceAmount, double angle, ForceType forceType) {
		addForceVectors(Math.cos(Math.toRadians(angle)) * forceAmount, Math.sin(Math.toRadians(angle)) * forceAmount, forceType);
	}
	
	public void addForceVectors(double xAmount, double yAmount, ForceType forceType) {
		this.motionX += Physics.getAcceleration(xAmount, mass, forceType);
		this.motionY += Physics.getAcceleration(yAmount, mass, forceType);
	}
	
	public boolean isFirstUpdate() {
		return lifetime <= 0;
	}
	
	public GameObject setPosition(double posX, double posY) {
		this.x = posX;
		this.y = posY;
		return this;
	}
	
	public GameObject setSprite(Sprite sprite) {
		this.sprite = sprite;
		return this;
	}
	
	public Sprite getSprite() {
		return sprite;
	}
	
	public boolean isStatic() {
		return isStatic;
	}
	
	public boolean canCollideCheck() {
		return collideCheck && getCollider() != null;
	}
	
	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}
	
	public void setCollided(boolean collided) {
		this.collided = collided;
	}
	
	public boolean isCollided() {
		return collided;
	}
	
	public void setCollider(BoxCollider collider) {
		this.collider = collider;
	}

	public BoxCollider getCollider() {
		return collider;
	}

	public GameObject rotate(double degrees) {
		this.rotation += degrees;
		return this;
	}
	
	public void setMass(double mass) {
		this.mass = mass;
	}
	
	public double velocity() {
		return Math.sqrt(motionX * motionX + motionY * motionY + motionZ * motionZ);
	}
	
	public double velocityDirection() {
		return Math.toDegrees(Math.atan(motionY / motionX));
	}
	
	public double getAngle(GameObject gameObject) {
		double angle = Math.toDegrees(Math.atan2(gameObject.y - y, gameObject.x - x));
	    if(angle < 0){
	        angle += 360;
	    }
	    return angle;
	}
	
	public int getLifetime() {
		return lifetime;
	}
	
	public void setRotation(double rotation) {
		this.rotation = rotation;
	}
	
	public double getDistance(GameObject gameObject) {
		double x = this.x - gameObject.x;
		double y = this.y - gameObject.y;
		double z = this.z - gameObject.z;
		return Math.sqrt(x * x + y * y + z * z);
	}
	
}
