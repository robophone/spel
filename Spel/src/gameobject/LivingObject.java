package gameobject;

public class LivingObject extends GameObject {
	
	private double maxHealth, health;

	public LivingObject(double x, double y, double z, double maxHealth) {
		super(x, y, z);
		this.maxHealth = maxHealth;
		this.health = maxHealth;
	}
	
	@Override
	public void update() {
		super.update();
		livingUpdate();
	}
	
	public void livingUpdate() {
		
	}
	
	public void setHealth(double health) {
		this.health = health;
	}
	
	public double getHealth() {
		return health;
	}
	
	public double getMaxHealth() {
		return maxHealth;
	}
	
	public double damage(double health) {
		this.health -= health;
		if(this.health <= 0) {
			this.health = 0;
		}
		return this.health;
	}

}
