package gameobject;

import renderer.Sprites;

public class StarObject extends GameObject {

	public StarObject(int x, int y, int z) {
		super(x, y, z);
	}
	
	@Override
	public void init() {
		super.init();
		setSprite(Sprites.STAR_SPRITE);
		setStatic(true);
	}
	
}
