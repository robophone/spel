package gameobject;

import java.awt.geom.Point2D;

import gameobject.weapon.MissileObject;
import physics.ForceType;
import physics.collider.BoxCollider;
import util.RandomUtil;
import util.Utils;

public class PlayerObjectSP extends PlayerObject {

	public PlayerObjectSP(double x, double y, double z) {
		super(x, y, z, 20);
	}
	
	@Override
	public void init() {
		super.init();
		setMass(3);
	}
	
	@Override
	public void livingUpdate() {
		super.livingUpdate();
		
		if (game.keybinds().keyBindRight.pressed) {
			motionRotation += 2;
		}
		if (game.keybinds().keyBindLeft.pressed) {
			motionRotation -= 2;
		}
		if (game.keybinds().keyBindUp.pressed) {
			addForceAngle(1, rotation, ForceType.USE_MASS);
			for(int i = 0; i < 1; i++) {
				Point2D.Double forward = Utils.forward(rotation, 7.5);
				SmokeParticle particle = new SmokeParticle(x - forward.x, y - forward.y, z, 20);
				particle.addForceVectors(motionX, motionY, ForceType.IGNORE_MASS);
				particle.addForceAngle(6, rotation + 180 + RandomUtil.random().nextInt(11) - 5, ForceType.USE_MASS);
				particle.setRotation(rotation);
				game.world.addObj(particle);
			}
		}
		if (game.keybinds().keyBindDown.pressed) {
			
		}
		if(game.keybinds().keyBindSpace.pressed) {
			Point2D.Double forward = Utils.forward(rotation, 12);
			MissileObject missile = new MissileObject(x + motionX - forward.x, y + motionY - forward.y, z, 20, this, this);
			missile.addForceVectors(motionX, motionY, ForceType.IGNORE_MASS);
			missile.setRotation(rotation);
			game.world.addObj(missile);
		}
	}

}
