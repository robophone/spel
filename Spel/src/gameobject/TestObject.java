package gameobject;

import physics.collider.BoxCollider;
import renderer.Sprites;

public class TestObject extends GameObject {

	public TestObject(int x, int y) {
		super(x, y);
	}
	
	public TestObject(int x, int y, int z) {
		super(x, y, z);
	}
	
	@Override
	public void init() {
		super.init();
		setSprite(Sprites.TEST_SPRITE);
		setCollider(new BoxCollider(x, y, z, getSprite().getScaledWidth(), getSprite().getScaledHeight()));
	}
	
	@Override
	public void update() {
		motionY -= this.y * 0.001;
		
		super.update();
	}
	
}
