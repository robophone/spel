package gameobject;

public class Particle extends GameObject {

	public Particle(double x, double y, double z, int lifeTime) {
		super(x, y, z);
	}
	
	@Override
	public void update() {
		super.update();
		if(getLifetime() > 100) {
			game.world.removeObj(this);
		}
	}

}
