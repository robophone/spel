package gameobject;

import renderer.Sprites;

public class SmokeParticle extends Particle {

	public SmokeParticle(double x, double y, double z, int lifeTime) {
		super(x, y, z, lifeTime);
	}
	
	@Override
	public void init() {
		super.init();
		setSprite(Sprites.SMOKE_SPRITE);
	}
	
}
