package gameobject.weapon;

import gameobject.GameObject;
import gameobject.LivingObject;

public class Weapon extends GameObject {
	
	private double damage;
	private GameObject firingObj;

	public Weapon(double x, double y, double z, double damage, GameObject firingObj) {
		super(x, y, z);
		this.damage = damage;
		this.firingObj = firingObj;
	}
	
	@Override
	public void onCollision(GameObject obj) {
		super.onCollision(obj);
		
		if(obj.equals(firingObj)) {
			return;
		}
		
		if(obj instanceof LivingObject) {
			LivingObject livingObj = (LivingObject)obj;
			livingObj.damage(damage);
		}
		
	}
	
	public double getDamage() {
		return damage;
	}
	
	public void setDamage(double damage) {
		this.damage = damage;
	}

}
