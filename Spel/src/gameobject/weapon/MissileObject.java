package gameobject.weapon;

import java.awt.geom.Point2D;

import gameobject.GameObject;
import gameobject.SmokeParticle;
import physics.ForceType;
import renderer.Sprites;
import util.RandomUtil;
import util.Utils;

public class MissileObject extends Weapon {
	
	private GameObject target;

	public MissileObject(double x, double y, double z, double damage, GameObject firingObj, GameObject target) {
		super(x, y, z, damage, firingObj);
		this.target = target;
	}
	
	@Override
	public void init() {
		super.init();
		setSprite(Sprites.BULLET_SPRITE);
	}
	
	@Override
	public void update() {
		super.update();
		
		rotation = getAngle(target);
		
		this.addForceAngle(2, rotation, ForceType.USE_MASS);
		
		Point2D.Double forward = Utils.forward(rotation, 7.5);
		SmokeParticle particle = new SmokeParticle(x + motionX - forward.x, y + motionY - forward.y, z, 20);
		particle.addForceVectors(motionX, motionY, ForceType.IGNORE_MASS);
		particle.addForceAngle(6, rotation + 180 + RandomUtil.random().nextInt(11) - 5, ForceType.USE_MASS);
		particle.setRotation(rotation);
		game.world.addObj(particle);
	}
	
}
