package window;

import java.awt.Canvas;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

import main.Game;

public class GameCanvas extends Canvas {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BufferStrategy bufferStrategy;

	public GameCanvas(int width, int height) {
		setBounds(0, 0, width, height);
		setIgnoreRepaint(true);
	}

	public void setupBufferStrategy() {
		createBufferStrategy(2);
		bufferStrategy = getBufferStrategy();
	}
	
	public void render(double interpolation) {
		Graphics2D g = (Graphics2D) bufferStrategy.getDrawGraphics();
		g.clearRect(0, 0, WIDTH, HEIGHT);
		Game.getInstance().render(g, interpolation);
		g.dispose();
		bufferStrategy.show();
	}
	
}
