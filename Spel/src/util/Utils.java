package util;

import java.awt.geom.Point2D;

import gameobject.GameObject;
import main.Game;

public class Utils {
	
	public static Point2D.Double forward(double angle, double factor) {
		return new Point2D.Double(Math.cos(Math.toRadians(angle)) * factor, Math.sin(Math.toRadians(angle)) * factor);
	}
	
	public static GameObject getClosestGameObject(GameObject gameObject, double distance) {
		double minDist = distance;
		GameObject currentObj = null;
		for(GameObject obj : Game.getInstance().world.gameObjects) {
			if(obj.equals(gameObject)) {
				continue;
			}
			if(obj.getDistance(gameObject) < minDist) {
				currentObj = obj;
				minDist = obj.getDistance(gameObject);
			}
		}
		return currentObj;
	}
	
}
