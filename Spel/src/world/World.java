package world;

import java.util.ArrayList;

import gameobject.GameObject;
import gameobject.PlayerObjectSP;
import gameobject.StarObject;
import gameobject.TestObject;
import main.Game;
import util.RandomUtil;

public class World {
	
	public ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
	public ArrayList<GameObject> removingGameObjects = new ArrayList<GameObject>();
	public ArrayList<GameObject> addingGameObjects = new ArrayList<GameObject>();
	
	public ArrayList<GameObject> decorations = new ArrayList<GameObject>();
	
	public World() {
		addStars();
		
		addingGameObjects.add(new TestObject(0, -80, 0).rotate(0));
		addingGameObjects.add(new TestObject(0, 80, 0).rotate(45));
		
		PlayerObjectSP player = new PlayerObjectSP(0, 0, 0);

		Game.getInstance().player = player;
		addingGameObjects.add(player);
		Game.getInstance().renderer.renderViewObject = player;
		
//		Game.getInstance().renderer.renderViewObject = addingGameObjects.get(1);
	}
	
	public void addStars() {
		int bound = 10000;
		int zBound = 30000;
		int zOffset = 40000;
		for(int i = 0; i < 100; i++) {
			decorations.add(new StarObject(RandomUtil.random().nextInt(bound * 2 + 1) - bound, RandomUtil.random().nextInt(bound * 2 + 1) - bound, RandomUtil.random().nextInt(zBound + 1) - zOffset));
		}
	}
	
	public void update() {

		for(GameObject gameObject : decorations) {
			gameObject.preUpdate();
		}
			
		for(GameObject gameObject : gameObjects) {
			gameObject.preUpdate();
			gameObject.update();
			gameObject.physicsUpdate();
		}
		
		for(GameObject gameObject : gameObjects) {
			gameObject.setCollided(false);
		}
		for(GameObject gameObject : gameObjects) {
			gameObject.collisionUpdate();
		}
		
		addGameObjects();
		removeGameObjects();
		
	}
	
	private void addGameObjects() {
		gameObjects.addAll(addingGameObjects);
		addingGameObjects.clear();
	}
	
	private void removeGameObjects() {
		gameObjects.removeAll(removingGameObjects);
		removingGameObjects.clear();
	}
	
	public void addObj(GameObject obj) {
		addingGameObjects.add(obj);
	}
	
	public void removeObj(GameObject obj) {
		removingGameObjects.add(obj);
	}
	
}
