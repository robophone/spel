package main;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JFrame;

import file.FileManager;
import font.Fonts;
import gameobject.PlayerObjectSP;
import gui.GuiMain;
import gui.GuiPlaying;
import gui.GuiScreen;
import renderer.Renderer;
import setting.GameSettings;
import setting.GameSettings.Keybinds;
import window.GameCanvas;
import window.GameFrame;
import world.World;

public class Game {
	
	public static void main(String[] args) { game = new Game(); game.start(); }
	
	public static Game game;
	
	public GameFrame frame;
	public GameCanvas canvas;
	
	public FileManager fileManager;
	
	public World world;
	public Renderer renderer;
	public GameSettings gameSettings;
	
	public PlayerObjectSP player;
	
	public static int width = 1000;
	public static int height = 700;

	private boolean running = false;
	private boolean paused = false;
	private int frameCount = 0;
	private int fpsCounter = 0;
	
	private double UPDATE_FPS = 20.0;
	private double TIME_BETWEEN_UPDATES = 1000000000 / UPDATE_FPS;

	public GuiScreen currentScreen;
	
	private void start() {
		frame = new GameFrame("Game");
		frame.setBounds(frame.getX(), frame.getY(), width, height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		canvas = new GameCanvas(width, height);
		frame.add(canvas);
		frame.setVisible(true);
		canvas.setupBufferStrategy();
		
		canvas.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);
				onMousePressed(e);
			}
			
		});
		
		canvas.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				onKeyUp(e);
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				onKeyDown(e);
			}
		});
		
		canvas.addMouseWheelListener(new MouseWheelListener() {
			
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				onMouseWheel(e);
			}
		});
		
		frame.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				onResize(e.getComponent().getWidth(), e.getComponent().getHeight());
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				
			}
		});
		
		gameSettings = new GameSettings();
		
		fileManager = new FileManager();
		Fonts.loadFonts();
		
		displayGuiScreen(new GuiMain());
		
		renderer = new Renderer();
		
		running = true;
		startGameLoop();
	}

	public static Game getInstance() {
		return game;
	}
	
	public void render(double interpolation) {
		canvas.render(interpolation);
	}
	
	public void render(Graphics2D g, double interpolation) {
		
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		
		g.setFont(Fonts.font18);
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		g.setColor(Color.BLACK);
		
		renderer.render(g, interpolation);
		
	}
	
	public void update() {
		UPDATE_FPS = 20.0;
		TIME_BETWEEN_UPDATES = 1000000000 / UPDATE_FPS;
		if(currentScreen != null) {
			currentScreen.update();
		}
		if(world != null) {
			world.update();
		}
		renderer.update();
	}
	
	public void displayGuiScreen(GuiScreen guiScreen) {
		this.currentScreen = guiScreen;
		this.currentScreen.init();
	}
	
	public void newWorld() {
		world = new World();
		displayGuiScreen(new GuiPlaying());
	}
	
	public void onResize(int width, int height) {
		this.width = width;
		this.height = height;
		
		if(currentScreen != null) {
			currentScreen.onResize(width, height);
		}
	}
	
	public void onMousePressed(MouseEvent e) {
		if(currentScreen != null) {
			currentScreen.mouseClicked(e.getX(), e.getY(), e.getButton());
		}
	}
	
	public void onMouseWheel(MouseWheelEvent e) {
		renderer.onMouseWheel(e);
	}
	
	public void onKeyDown(KeyEvent e) {
		gameSettings.keybinds.onKeyPress(e);
	}
	
	public void onKeyUp(KeyEvent e) {
		gameSettings.keybinds.onKeyRelease(e);
	}
	
	//Jag skiddade denna kod

	// Starts a new thread and runs the game loop in it.
	public void startGameLoop() {
		Thread loop = new Thread() {
			public void run() {
				gameLoop();
			}
		};
		loop.start();
	}

	// Only run this in another Thread!
	private void gameLoop() {
		// At the very most we will update the game this many times before a new render.
		// If you're worried about visual hitches more than perfect timing, set this to
		// 1.
		final int MAX_UPDATES_BEFORE_RENDER = 5;
		// We will need the last update time.
		double lastUpdateTime = System.nanoTime();
		// Store the last time we rendered.
		double lastRenderTime = System.nanoTime();

		// If we are able to get as high as this FPS, don't render again.
		final double TARGET_FPS = 60;
		final double TARGET_TIME_BETWEEN_RENDERS = 1000000000 / TARGET_FPS;

		// Simple way of finding FPS.
		int lastSecondTime = (int) (lastUpdateTime / 1000000000);

		while (running) {
			double now = System.nanoTime();
			int updateCount = 0;

			if (!paused) {
				// Do as many game updates as we need to, potentially playing catchup.
				while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER) {
					update();
					lastUpdateTime += TIME_BETWEEN_UPDATES;
					updateCount++;
				}

				// If for some reason an update takes forever, we don't want to do an insane
				// number of catchups.
				// If you were doing some sort of game that needed to keep EXACT time, you would
				// get rid of this.
				if (now - lastUpdateTime > TIME_BETWEEN_UPDATES) {
					lastUpdateTime = now - TIME_BETWEEN_UPDATES;
				}

				// Render. To do so, we need to calculate interpolation for a smooth render.
				double interpolation = Math.min(1.0f, (float) ((now - lastUpdateTime) / TIME_BETWEEN_UPDATES));
				render(interpolation);
				lastRenderTime = now;

				// Update the frames we got.
				int thisSecond = (int) (lastUpdateTime / 1000000000);
				if (thisSecond > lastSecondTime) {
//					System.out.println("NEW SECOND " + thisSecond + " " + frameCount);
					fpsCounter = frameCount;
					frameCount = 0;
					lastSecondTime = thisSecond;
				}

				// Yield until it has been at least the target time between renders. This saves
				// the CPU from hogging.
				while (now - lastRenderTime < TARGET_TIME_BETWEEN_RENDERS
						&& now - lastUpdateTime < TIME_BETWEEN_UPDATES) {
					Thread.yield();

					// This stops the app from consuming all your CPU. It makes this slightly less
					// accurate, but is worth it.
					// You can remove this line and it will still work (better), your CPU just
					// climbs on certain OSes.
					// FYI on some OS's this can cause pretty bad stuttering. Scroll down and have a
					// look at different peoples' solutions to this.
					try {
						Thread.sleep(1);
					} catch (Exception e) {
					}

					now = System.nanoTime();
				}
			}
		}
	}
	
	public Keybinds keybinds() {
		return gameSettings.keybinds;
	}

}
