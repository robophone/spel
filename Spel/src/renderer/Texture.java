package renderer;

import java.awt.image.BufferedImage;

import setting.SettingTarget;

public class Texture {
	
	@SettingTarget
	public static final boolean LOAD_ON_INIT = false; //TRUE: Laggar varje g�ng en texture �r skapad och inte cachad, anv�nder mer minne - FALSE: laggar bara f�rsta g�ngen den anv�nds, sparar minne

	private final String resourceName;
	
	private BufferedImage bufferedImage;
	
	private boolean loaded;
	
	public Texture(String resourceName) {
		this.resourceName = resourceName;
		if(LOAD_ON_INIT) {
			getImage();
		}
	}
	
	public BufferedImage getImage() {
		if(loaded) {
			return bufferedImage;
		}
		loaded = true;
		bufferedImage = ImgCache.tryLoadCache(resourceName);
		return bufferedImage;
	}
	
	public String getResourceName() {
		return resourceName;
	}
	
}
