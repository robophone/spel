package renderer;

import java.awt.image.BufferedImage;

public class Sprite {
	
	private double scale = 1;
	
	private int currentTexture = 0;
	
	private Texture[] textures;
	
	public Sprite(String ...resourceNames) {
		Texture[] textures = new Texture[resourceNames.length];
		for(int i = 0; i < resourceNames.length; i++) {
			textures[i] = new Texture("img/" + resourceNames[i]);
		}
		this.textures = textures;
	}
	
	public Sprite(double scale, String ...resourceNames) {
		this(resourceNames);
		this.scale = scale;
	}
	
	public Sprite(Texture ...textures) {
		this.textures = textures;
	}
	
	public Texture[] getTextures() {
		return textures;
	}
	
	public BufferedImage getTextureImage(int textureIndex) {
		return textures[textureIndex].getImage();
	}
	
	public Texture getTexture() {
		return textures[currentTexture];
	}
	
	public BufferedImage getTextureImage() {
		return textures[currentTexture].getImage();
	}
	
	public boolean hasAnimation() {
		return textures.length > 1;
	}
	
	public int getScaledHeight() {
		return (int)(textures[currentTexture].getImage().getHeight() * scale);
	}
	
	public int getScaledWidth() {
		return (int)(textures[currentTexture].getImage().getWidth() * scale);
	}
	
	public double getScale() {
		return scale;
	}
	
}
