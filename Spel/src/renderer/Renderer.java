package renderer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import gameobject.GameObject;
import gui.Gui;
import main.Game;
import physics.collider.BoxCollider;
import setting.SettingTarget;
import world.World;

public class Renderer extends Gui {
	
	@SettingTarget
	public static boolean renderCollisionInfo = false;
	
	public GameObject renderViewObject;
	
	public double preCameraX, cameraX, cameraXTarget;
	public double preCameraY, cameraY, cameraYTarget;
	public double preCameraZ, cameraZ, cameraZTarget = 500;
	
	public double preFovLevel, fovLevel, fovTarget = 0;
	
	public double preCameraRotation, cameraRotation, cameraRotationTarget;
	
	public static final double NEAR_PLANE = 0.01 * 200d;
	public static final double FAR_PLANE = 4000;
	
	public void update() {
		Game game = Game.getInstance();
		
		preFovLevel = fovLevel;
		preCameraX = cameraX;
		preCameraY = cameraY;
		preCameraZ = cameraZ;
		preCameraRotation = cameraRotation;
		
//		if(game.keybinds().keyBindRight.pressed) {
//			cameraXTarget += 10;
//		}
//		if(game.keybinds().keyBindLeft.pressed) {
//			cameraXTarget -= 10;
//		}
//		if(game.keybinds().keyBindDown.pressed) {
//			cameraYTarget += 10;
//		}
//		if(game.keybinds().keyBindUp.pressed) {
//			cameraYTarget -= 10;
//		}
		
		if(renderViewObject != null) {
			this.cameraX = renderViewObject.x;
			this.cameraY = renderViewObject.y;
//			this.cameraZ = renderViewObject.z + 500;
			this.cameraRotationTarget = renderViewObject.rotation + 90;
			fovTarget = -renderViewObject.velocity() / 400d;
		}
		else {
			cameraX -= (cameraX - cameraXTarget) / 4;
			cameraY -= (cameraY - cameraYTarget) / 4;
		}
		cameraZ -= (cameraZ - cameraZTarget) / 4;
		cameraRotation -= (cameraRotation - cameraRotationTarget) / 4;
		
		fovLevel -= (fovLevel - fovTarget) / 10;
	}
	
	public void render(Graphics2D g, double interpolation) {
		
		Game game = Game.getInstance();
		
		renderWorld(g, interpolation);
		
		if(game.currentScreen != null) {
			game.currentScreen.render(g, interpolation);
		}
		
	}
	
	public void renderWorld(Graphics2D g, double interpolation) {
		
		Game game = Game.getInstance();
		World world = game.world;
		
		if(world == null) {
			return;
		}
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, Game.width, Game.height);
		
		renderGameObjects(g, interpolation);
		
	}
	
	public void renderGameObjects(Graphics2D g, double interpolation) {
		
		Game game = Game.getInstance();
		World world = game.world;
		
		GameObject[] objects = new GameObject[world.gameObjects.size() + world.decorations.size()];
		
		int size = 0;
		for(int i = 0; i < world.decorations.size(); i++) {
			objects[i] = world.decorations.get(i);
			if(i == world.decorations.size() - 1) {
				size = i + 1;
			}
		}
		
		for(int i = 0; i < world.gameObjects.size(); i++) {
			objects[size + i] = world.gameObjects.get(i);
		}
		
		g.setColor(Color.WHITE);
		
		AffineTransform rootTransform = new AffineTransform();
		rootTransform.translate(Game.width / 2, Game.height / 2);
		rootTransform.rotate(-Math.toRadians(interpolate(preCameraRotation, cameraRotation, interpolation)));
		
		double fixedCameraX = interpolate(preCameraX, cameraX, interpolation);
		double fixedCameraY = interpolate(preCameraY, cameraY, interpolation);
		double fixedCameraZ = interpolate(preCameraZ / 200d, cameraZ / 200d, interpolation);
//		fixedCameraX = cameraX;
//		fixedCameraY = cameraY;
		
		for(GameObject obj : objects) {
			
			if(obj.isFirstUpdate()) {
				continue;
			}
			
			if(fixedCameraZ - NEAR_PLANE / 200d < obj.z / 200d
					/*|| fixedCameraZ - FAR_PLANE / 200d > obj.z / 200d*/) {
				continue;
			}
			
			double fixedZoom = -Math.pow(1.3333, preFovLevel + (fovLevel - preFovLevel) * interpolation) * 
					(1d / ((obj.preZ + (obj.z - obj.preZ) * interpolation) / 200d - fixedCameraZ) * 8d);
			
			Sprite sprite = obj.getSprite();
			double width = sprite.getScaledWidth();
			double height = sprite.getScaledHeight();
			
			AffineTransform transform = new AffineTransform(rootTransform);
			
			double x = (obj.preX + (obj.x - obj.preX) * interpolation - fixedCameraX - width / 2) * fixedZoom;
			double y = (obj.preY + (obj.y - obj.preY) * interpolation - fixedCameraY - height / 2) * fixedZoom;
			
			
			transform.translate(x, y);
			transform.scale(fixedZoom * sprite.getScale(), fixedZoom * sprite.getScale());
			transform.rotate(Math.toRadians(obj.preRotation + (obj.rotation - obj.preRotation) * interpolation), sprite.getTextureImage().getWidth() / 2d, sprite.getTextureImage().getHeight() / 2d);
			
			g.drawImage(sprite.getTextureImage(), transform, game.canvas);
			
		}
		
		if(renderCollisionInfo) {
			for(GameObject obj1 : objects) {
				if(!obj1.canCollideCheck() || obj1.isCollided()) {
					continue;
				}
				
				BoxCollider obj = obj1.getCollider();
				
				if(fixedCameraZ - NEAR_PLANE / 200d < obj.z / 200d
						/*|| fixedCameraZ - FAR_PLANE / 200d > obj.z / 200d*/) {
					continue;
				}
				
				double fixedZoom = -Math.pow(1.3333, preFovLevel + (fovLevel - preFovLevel) * interpolation) * 
						(1d / ((obj.z) / 200d - fixedCameraZ) * 8d);
				
				Sprite sprite = Sprites.TEST_SPRITE;
				double width = sprite.getScaledWidth();
				double height = sprite.getScaledHeight();
				
				AffineTransform transform = new AffineTransform(rootTransform);
				
				double x = (obj.x - fixedCameraX - width / 2) * fixedZoom;
				double y = (obj.y - fixedCameraY - height / 2) * fixedZoom;
				
				transform.translate(x, y);
				transform.scale(fixedZoom * sprite.getScale(), fixedZoom * sprite.getScale());
				transform.rotate(Math.toRadians(obj.rotation), width / 2, height / 2);
				
				g.drawImage(sprite.getTextureImage(), transform, game.canvas);
				
			}
		}
		
	}
	
	public Point2D.Double getPointOnScreen(int gameX, int gameY) {
		return null;
	}
	
	public Point2D.Double getPointInGame(int screenX, int screenY) {
		return null;
	}

	public void onMouseWheel(MouseWheelEvent e) {
		this.cameraZTarget += e.getWheelRotation() * 100d;
	}
	
	private double interpolate(double pre, double now, double interpolation) {
		return pre + (now - pre) * interpolation;
	}
	
}
