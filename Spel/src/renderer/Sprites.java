package renderer;

public class Sprites {

	public static final Sprite TEST_SPRITE = new Sprite("test/testObj.png");
	public static final Sprite STAR_SPRITE = new Sprite(1, "gameobject/star/star.png");
	public static final Sprite SMOKE_SPRITE = new Sprite(0.04, "gameobject/star/star.png");
	public static final Sprite SHIP_SPRITE = new Sprite(0.4, "gameobject/ship/ship_01.png");
	public static final Sprite BULLET_SPRITE = new Sprite(0.4, "gameobject/ship/ship_01.png");
	
}
