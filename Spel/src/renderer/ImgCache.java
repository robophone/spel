package renderer;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import main.Game;

public class ImgCache {
	
	private static HashMap<String, BufferedImage> imgCacheList = new HashMap<String, BufferedImage>();
	
	public static BufferedImage tryLoadCache(String resourceName) {
		BufferedImage img = imgCacheList.get(resourceName);
		boolean usedCache = true;
		if(img == null) {
			try {
				img = ImageIO.read(Game.class.getClassLoader().getResource(resourceName));
			} catch (IOException e) {
				e.printStackTrace();
			}
			imgCacheList.put(resourceName, img);
			usedCache = false;
		}
		System.out.println("Cache access : " + usedCache + ", " + resourceName);
		return img;
	}
	
}
