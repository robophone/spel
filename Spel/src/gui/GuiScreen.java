package gui;

import java.awt.Graphics2D;
import java.util.ArrayList;

import main.Game;

public class GuiScreen extends Gui {
	
	protected int width;
	protected int height;
	
	protected ArrayList<GuiButton> buttonList = new ArrayList<GuiButton>();
	
	public void init() {
		this.buttonList.clear();
		this.width = Game.width;
		this.height = Game.height;
	}
	
	public void update() {
		for(GuiButton button : buttonList) {
			button.update();
		}
	}
	
	public void render(Graphics2D g, double interpolation) {
		for(GuiButton button : buttonList) {
			button.render(g, interpolation);
		}
	}
	
	public void onResize(int width, int height) {
		this.width = width;
		this.height = height;
		init();
	}
	
	protected void addButton(GuiButton button) {
		this.buttonList.add(button);
	}
	
	public void mouseClicked(int x, int y, int mouseButton) {
		for(GuiButton button : buttonList) {
			if(button.getX() < x
					&& button.getX() + button.getWidth() > x
					&& button.getY() < y
					&& button.getY() + button.getHeight() > y)
			button.onClicked(mouseButton);
		}
	}
	
}
