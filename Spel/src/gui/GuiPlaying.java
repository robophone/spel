package gui;

import java.awt.Color;
import java.awt.Graphics2D;

import main.Game;

public class GuiPlaying extends GuiScreen {
	
	@Override
	public void render(Graphics2D g, double interpolation) {
		super.render(g, interpolation);
		
		drawCenteredString("Zoom: " + Math.round(Game.getInstance().renderer.fovLevel * 10d) / 10d, width / 2, 20, Color.WHITE, g);
		drawCenteredString("Camera x: " + Math.round(Game.getInstance().renderer.cameraX * 10d) / 10d, width / 2, 40, Color.WHITE, g);
		drawCenteredString("Camera y: " + Math.round(Game.getInstance().renderer.cameraY * 10d) / 10d, width / 2, 60, Color.WHITE, g);
		drawCenteredString("Camera z: " + Math.round(Game.getInstance().renderer.cameraZ * 10d) / 10d, width / 2, 80, Color.WHITE, g);
	}
	
}
