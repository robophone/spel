package gui;

import java.awt.Color;
import java.awt.Graphics2D;

import main.Game;
import renderer.Sprite;

public class Gui {
	
	protected void drawSprite(Sprite sprite, double x, double y, int width, int height, Graphics2D g) {
		g.drawImage(sprite.getTextureImage(), (int)x, (int)y, width, height, Game.getInstance().canvas);
	}
	
	protected void drawCenteredString(String text, int x, int y, Color color, Graphics2D g) {
		Color colorBefore = g.getColor();
		g.setColor(color);
		g.drawString(text, x - g.getFontMetrics().stringWidth(text) / 2, y);
		g.setColor(colorBefore);
	}
	
	protected void drawCenteredString(String text, int x, int y, Graphics2D g) {
		drawCenteredString(text, x, y, g.getColor(), g);
	}

}
