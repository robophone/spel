package gui;

import java.awt.Color;
import java.awt.Graphics2D;

public abstract class GuiButton extends Gui {
	
	private String text;
	
	private int x;
	private int y;
	
	private int width;
	private int height;
	
	public GuiButton(String text, int x, int y) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.width = 300;
		this.height = 30;
	}
	
	public GuiButton(String text, int x, int y, int width, int height) {
		this(text, x, y);
		this.width = width;
		this.height = height;
	}
	
	public abstract void onClicked(int button);
	
	public void update() {
		
	}
	
	public void render(Graphics2D g, double interpolation) {
		g.setColor(Color.BLACK);
		g.fillRect(x, y, width, height);
		drawCenteredString(text, x + width / 2, y + height - 8, Color.WHITE, g);
	}
	
	public String getText() {
		return text;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
}
