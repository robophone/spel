package gui;

import java.awt.Color;
import java.awt.Graphics2D;

import main.Game;

public class GuiMain extends GuiScreen {
	
	@Override
	public void init() {
		super.init();
		
		addButton(new GuiButton("Play", width / 2 - 150, 100) {
			@Override
			public void onClicked(int button) {
				Game.getInstance().newWorld();
			}
		});
	}
	
	@Override
	public void render(Graphics2D g, double interpolation) {
		
		drawCenteredString("Cool rocket game", width / 2, 50, Color.BLACK, g);
		
		super.render(g, interpolation);
	}
	
}
