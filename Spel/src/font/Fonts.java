package font;

import java.awt.Font;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;

import main.Game;

public class Fonts {
	
	private static Font fontFromFile;
	
	public static Font font18;

	public static void loadFonts() {
		
		fontFromFile = unpackFont("font.ttf", Game.getInstance().fileManager.fontFile.getFile());
		
		font18 = fontFromFile.deriveFont(Font.PLAIN, 18);
		
	}
	
	private static Font unpackFont(String resourceName, File srcFile) {
		try {
			DataInputStream dis = new DataInputStream(Game.class.getClassLoader().getResourceAsStream(resourceName));
			FileOutputStream outputStream = new FileOutputStream(srcFile);

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = dis.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.close();
			dis.close();
			
			return Font.createFont(Font.TRUETYPE_FONT, srcFile);
		}
		catch(Exception e) {
			System.out.println("Error unpacking font: " + resourceName);
			e.printStackTrace();
		}
		return null;
	}
	
}